package cn.tedu.sp04.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 声明式客户端接口,远程调用接口
 * 三步配置
 * 1.调用哪个模块
 * 2.调用哪个路径
 * 3.提交什么参数
 */
@FeignClient("item-service") //从注册表得到该模块的服务器地址列表
public interface ItemClient {
    /**
     * spring mvc 注解在这里起到的作用数相反的
     *      - controller 响应客户端调用
     *      - 客户端接口,向服务器发送调用
     * @param orderId
     * @return
     */
    @GetMapping("/{orderId}")
    JsonResult<List<Item>> getItems(@PathVariable("orderId") String orderId);

    @GetMapping("/decreaseNumber")
    JsonResult<?> decreaseNumber(@RequestBody List<Item> items);
}
