package cn.tedu.sp04.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.OrderService;
import cn.tedu.web.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    //获取订单
    @GetMapping("/{orderId}")
    public JsonResult<Order> getOrder(@PathVariable("orderId") String orderId){
        Order order = orderService.getOrder(orderId);
        return JsonResult.build().code(200).data(order);
    }

    //添加订单
    @GetMapping("/add")
    public JsonResult<?> add(){
        Order order = new Order("sajkhduisadhfsdfko", Arrays.asList(new Item[]{
                new Item(1,"商品1",3),
                new Item(2,"商品2",1),
                new Item(3,"商品3",5),
                new Item(4,"商品4",3),
                new Item(5,"商品5",2)
        }), new User(8,null,null));
        orderService.addOrder(order);
        return JsonResult.build().code(200).msg("添加订单成功");
    }

    //处理浏览器自动请求的图标文件
    @GetMapping("/favicon.ico")
    public void ico(){

    }
}
