package cn.tedu.sp06.fb;

import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Component
public class ItemFb implements FallbackProvider {
    /**
     * 针对哪个模块,应用当前降级类
     * item-service :只针对商品降级
     *      *       :对所有模块都应用当前降级类
     *      null    :对所有模块都应用当前降级类
     * @return
     */
    @Override
    public String getRoute() {
        return "order-service";
    }

    //发送给客户的降级响应
    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        return new ClientHttpResponse() {
            //HttpStatus对象,封装状态码和状态文本
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR;
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.value();
            }

            @Override
            public String getStatusText() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
            }

            @Override
            public void close() {
                //本方法用来关闭下面的输入流,释放系统资源
                //现在所用的ByteArrayInputStream只封装虚拟机中的内存数组,不占用系统的底层资源,所以不用关流
            }

            @Override
            public InputStream getBody() throws IOException {
                // JsonResult - {code:500,msg:调用后台服务失败,data:null}
                String json = JsonResult.build().code(500).msg("调用后台服务失败").toString();
                return new ByteArrayInputStream(json.getBytes("UTF-8"));
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders h = new HttpHeaders();
                h.set("Content-Type", "application/json;charset=UTF-8");
                return h;
            }
        };
    }
}
