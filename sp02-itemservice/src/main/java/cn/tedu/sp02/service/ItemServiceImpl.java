package cn.tedu.sp02.service;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;
@Service
@Slf4j
public class ItemServiceImpl implements ItemService {
    @Override
    public List<Item> getItems(String orderId) {
        log.info("获取订单的商品列表，orderId="+orderId);

        //Demo数据
        return Arrays.asList(new Item[]{
                new Item(1,"商品",1),
                new Item(2,"商品2",5),
                new Item(3,"商品3",2),
                new Item(4,"商品4",4),
                new Item(5,"商品5",1)
        });
    }

    @Override
    public void decreaseNumber(List<Item> items) {
        for (Item item : items){
            log.info("减少商品库存："+item);
        }
    }
}
